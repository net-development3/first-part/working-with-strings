﻿using System;

namespace WorkingWithStrings
{
    public static class UsingRanges
    {
        public static string GetStringWithAllChars(string str)
        {
            return str[..];
        }

        public static string GetStringWithoutFirstChar(string str)
        {
            return str[1..];
        }

        public static string GetStringWithoutTwoFirstChars(string str)
        {
            return str[2..];
        }

        public static string GetStringWithoutThreeFirstChars(string str)
        {
            return str[3..];
        }

        public static string GetStringWithoutLastChar(string str)
        {
            return str[.. (str.Length - 1)];
        }

        public static string GetStringWithoutTwoLastChars(string str)
        {
            return str[.. (str.Length - 2)];
        }

        public static string GetStringWithoutThreeLastChars(string str)
        {
            return str[.. (str.Length - 3)];
        }

        public static string GetStringWithoutFirstAndLastChars(string str)
        {
            return str[1.. (str.Length - 1)];
        }

        public static string GetStringWithoutTwoFirstAndTwoLastChars(string str)
        {
            return str[2.. (str.Length - 2)];
        }

        public static string GetStringWithoutThreeFirstAndThreeLastChars(string str)
        {
            return str[3.. (str.Length - 3)];
        }

        public static void GetProductionCodeDetails(string productionCode, out string regionCode, out string locationCode, out string dateCode, out string factoryCode)
        {
            regionCode = productionCode.Substring(0, 1);
            locationCode = productionCode.Substring(3, 2);
            dateCode = productionCode.Substring(7, 3);
            factoryCode = productionCode.Substring(productionCode.Length - 4);
        }

        public static void GetSerialNumberDetails(string serialNumber, out string countryCode, out string manufacturerCode, out string factoryCode, out string stationCode)
        {
            countryCode = serialNumber.Substring(serialNumber.Length - 9, 1);
            manufacturerCode = serialNumber.Substring(serialNumber.Length - 8, 2);
            factoryCode = serialNumber.Substring(serialNumber.Length - 5, 4);
            stationCode = serialNumber.Substring(serialNumber.Length - 1);
        }
    }
}
